import com.nidhogg.entity.CategoryEntity;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by Arthur on 21.05.2017.
 */
public class CategoryUITest extends AbstractUITest {

    private WebElement categoryMenu;
    private WebElement newBtn;
    private WebElement field;
    private WebElement btnSave;

    private int counter = 0;


    private void setValue(String value) {
        field.clear();
        field.sendKeys(value);
    }

    private void setCategory(CategoryEntity category) {
        newBtn = driver.findElement(By.id("newCategoryBtn"));
        newBtn.click();

        field = driver.findElement(By.id("categoryNameField"));
        btnSave = driver.findElement(By.id("saveCategoryFormBtn"));

        setValue(category.getName());
        btnSave.click();
        counter++;
    }

    @Test
    public void testCategoryPage() throws InterruptedException {
        driver.get(BASE_URL);

        WebDriverWait wait = new WebDriverWait(driver, 20);

        categoryMenu = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='v-menubar-menuitem']" +
                "[./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Hotel Categories')]")));
        categoryMenu.click();

        //Количество категорий в гриде до добавления новых
        int oldSize = driver.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class, 'v-grid-row')]")).size();

        setCategory(new CategoryEntity("Villa"));

        setCategory(new CategoryEntity("House"));

        //Количество категорий после добавления
        int newSize = driver.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class, 'v-grid-row')]")).size();

        Assert.assertTrue(newSize - counter == oldSize);
    }
}
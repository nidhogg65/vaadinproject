import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;


/**
 * Created by Arthur on 21.05.2017.
 */
public class AbstractUITest {
    protected WebDriver driver;
    protected static final String BASE_URL = "http://localhost:8079";

    public AbstractUITest() {
        System.setProperty("webdriver.opera.driver", "E:/DOWNLOADS/operadriver_win64/operadriver_win64/operadriver.exe");
    }

    @Before
    public void initDriver() throws InterruptedException {
        driver = new OperaDriver();
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}

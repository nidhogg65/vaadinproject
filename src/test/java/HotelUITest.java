import com.nidhogg.entity.CategoryEntity;
import com.nidhogg.entity.HotelEntity;
import com.nidhogg.entity.PaymentMethod;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Arthur on 21.05.2017.
 */
public class HotelUITest extends AbstractUITest {

    //Кнопки
    private WebElement hotelMenu;
    private WebElement newBtn;
    private WebElement btnSave;

    //Поля
    private WebElement fieldName;
    private WebElement fieldAddress;
    private WebElement fieldRating;
    private WebElement date;
    private WebElement fieldCategory;
    private WebElement fieldUrl;
    private WebElement fieldDescription;
    private WebElement fieldPaymentMethod;

    //Переходной элемент
    private WebElement element;

    private int counter = 0;


    //Задает значение переданному эелементу(полю)
    private void setValue(String value, WebElement field) {
        field.clear();
        field.sendKeys(value);
    }

    private void initElements() {
        fieldName = driver.findElement(By.id("name"));
        fieldAddress = driver.findElement(By.id("address"));
        fieldRating = driver.findElement(By.id("rating"));
        fieldUrl = driver.findElement(By.id("url"));
        fieldDescription = driver.findElement(By.id("description"));
        btnSave = driver.findElement(By.id("saveBtn"));
    }

    private void setHotel(HotelEntity hotel) {
        //Добавление 1го отеля
        newBtn = driver.findElement(By.id("newBtn"));
        newBtn.click();

        initElements();

        setValue(hotel.getName(), fieldName);
        setValue(hotel.getAddress(), fieldAddress);
        setValue(hotel.getRating() + "", fieldRating);

        element = driver.findElement(By.xpath("//button[@class='v-datefield-button']"));
        element.click();
        element = driver.findElement(By.className("v-datefield-calendarpanel-body"));
        date = element.findElement(By.xpath("//table/tbody/tr/td[@role='gridcell'][contains(./span/text(), '1')]"));
        date.click();

        fieldCategory = driver.findElement(By.className("v-select-select"));
        fieldCategory.click();

        element = driver.findElement(By.xpath("//option[@value='1']"));
        element.click();

        fieldPaymentMethod = driver.findElement(By.xpath("//input[@type='radio']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(fieldPaymentMethod).click().perform();

        element = driver.findElement(By.xpath("//input[@placeholder='Guaranty Deposit']"));
        setValue("10", element);

        setValue(hotel.getUrl(), fieldUrl);

        setValue(hotel.getDescription() != null ? hotel.getDescription(): "", fieldDescription);

        btnSave.click();

        counter++;
    }

    @Test
    public void testHotelPage() throws InterruptedException {
        driver.get(BASE_URL);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        //Переходит на страницу с отелями
        hotelMenu = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='v-menubar-menuitem']" +
                "[./span/@class='v-menubar-menuitem-caption'][contains(./span/text(), 'Hotel List')]")));
        hotelMenu.click();

        //Количество отелей в гриде до добавления новых
        int oldSize = driver.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class, 'v-grid-row')]")).size();

        setHotel(new HotelEntity("Prime Apartments 1", "Karla Marksa Street 6, Ленинский район",5,
                1000L, new CategoryEntity("House"), new PaymentMethod(10),
                "https://www.booking.com/hotel/by/prime-apartments-agency.ru.htm", "Good hotel"));
        setHotel(new HotelEntity("Flatcom Hostel", "Vulitsa Prititskogo 2, Bldg 1",3,
                1000L, new CategoryEntity("Hostel"), new PaymentMethod(10),
                "https://www.booking.com/hotel/by/fletkom.ru.html", "Good hostel"));

        //Количество отелей после добавления
        int newSize = driver.findElements(By.xpath("//tbody[@class='v-grid-body']/tr[contains(@class, 'v-grid-row')]")).size();

        Assert.assertTrue(newSize - counter == oldSize);
    }
}

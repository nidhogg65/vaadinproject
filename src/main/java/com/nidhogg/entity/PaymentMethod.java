package com.nidhogg.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Arthur on 19.05.2017.
 */

@SuppressWarnings("serial")
@Embeddable
public class PaymentMethod implements Serializable {

    private Integer guarantyFee;

    private Integer oldValue;

    @Column(table = "HOTEL", name = "GUARANTY_FEE")
    public Integer getGuarantyFee() {
        return guarantyFee;
    }

    public void setGuarantyFee(Integer guarantyFee) {
        this.guarantyFee = guarantyFee;
    }

    @Transient
    public Integer getOldValue() {
        return oldValue;
    }

    public void setOldValue(Integer oldValue) {
        this.oldValue = oldValue;
    }

    public PaymentMethod() {
        super();
    }

    public PaymentMethod(Integer value) {
        super();
        this.guarantyFee = value;
        this.oldValue = value;
    }
}

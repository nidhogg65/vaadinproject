package com.nidhogg.entity;

import javax.persistence.*;

/**
 * Created by Arthur on 12.05.2017.
 */
@Entity
@Table(name = "HOTEL", schema = "demo_hotels")
@NamedQuery(name = "HotelEntity.getAll", query = "from HotelEntity")
public class HotelEntity extends AbstractEntity {

    private String name;
    private String address;
    private int rating;
    private Long operatesFrom;
    private CategoryEntity category;
    private PaymentMethod paymentMethod;
    private String url;
    private String description;

    public HotelEntity() {
    }

    public HotelEntity(String name, String address, int rating, Long operatesFrom,
                       CategoryEntity category, PaymentMethod paymentMethod, String url, String description) {
        this.name = name;
        this.address = address;
        this.rating = rating;
        this.operatesFrom = operatesFrom;
        this.category = category;
        this.paymentMethod = paymentMethod;
        this.url = url;
        this.description = description;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ADDRESS")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "RATING")
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "OPERATES_FROM")
    public Long getOperatesFrom() {
        return operatesFrom;
    }

    public void setOperatesFrom(Long operatesFrom) {
        this.operatesFrom = operatesFrom;
    }

    @ManyToOne()
    @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID")
    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    @Embedded
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Basic
    @Column(name = "URL")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof HotelEntity)) return false;

        HotelEntity that = (HotelEntity) o;

        if (rating != that.rating) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (operatesFrom != null ? !operatesFrom.equals(that.operatesFrom) : that.operatesFrom != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (paymentMethod != null ? !paymentMethod.equals(that.paymentMethod) : that.paymentMethod != null)
            return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + rating;
        result = 31 * result + (operatesFrom != null ? operatesFrom.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (paymentMethod != null ? paymentMethod.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}

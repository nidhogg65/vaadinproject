package com.nidhogg.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Arthur on 13.05.2017.
 */
@MappedSuperclass
public class AbstractEntity implements Serializable, Cloneable {

    private Long id;
    private Long version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Version
    @Column(name = "OPTLOCK")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}

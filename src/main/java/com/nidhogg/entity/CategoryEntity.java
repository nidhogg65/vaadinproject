package com.nidhogg.entity;

import javax.persistence.*;

/**
 * Created by Arthur on 11.05.2017.
 */
@Entity
@Table(name = "category", schema = "demo_hotels")
@NamedQuery(name = "CategoryEntity.getAll", query = "from CategoryEntity")
public class CategoryEntity extends AbstractEntity {

    private String name;

    public CategoryEntity() {
    }

    public CategoryEntity(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "NAME", unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof CategoryEntity)) return false;

        CategoryEntity that = (CategoryEntity) o;

        return name.equals(that.getName());
    }

    @Override
    public String toString() {
        return name;
    }
}

package com.nidhogg.forms;

import com.vaadin.data.Binder;
import com.vaadin.ui.FormLayout;

/**
 * Created by Arthur on 31.05.2017.
 */
public abstract class AbstractForm<T> extends FormLayout {

    protected Binder<T> binder;

    abstract protected void setTooltips();

    abstract protected void bindFields();

    abstract public void setBean(T bean);

    abstract protected void save();
}

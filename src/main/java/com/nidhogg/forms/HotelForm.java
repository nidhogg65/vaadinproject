package com.nidhogg.forms;

import com.nidhogg.views.HotelView;
import com.nidhogg.UI.PaymentMethodCustomField;
import com.nidhogg.binding.DateConverter;
import com.nidhogg.entity.CategoryEntity;
import com.nidhogg.entity.HotelEntity;
import com.nidhogg.entity.PaymentMethod;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

/**
 * Created by Arthur on 17.05.2017.
 */

public class HotelForm extends AbstractForm<HotelEntity> {

    private HotelView view;
    private TextField name = new TextField("Name");
    private TextField address = new TextField("Address");
    private TextField rating = new TextField("Rating");
    private DateField operatesFrom = new DateField("Operates From");
    private NativeSelect<CategoryEntity> category = new NativeSelect<>("Category Entity");
    private PaymentMethodCustomField paymentMethod = new PaymentMethodCustomField("Payment method");
    private TextField url = new TextField("URL");
    private TextArea description = new TextArea("Description");

    private HotelEntity hotel;

    public HotelForm(HotelView view) {
        this.view = view;

        binder = new Binder<>(HotelEntity.class);

        setSizeUndefined();

        Button save = new Button("Save");
        save.setId("saveBtn");
        Button cancel = new Button("Cancel");

        save.addClickListener(clickEvent -> {
            save();
            view.updateList();
        });
        cancel.addClickListener(clickEvent -> setVisible(false));

        //Надо фиксить
        /*paymentMethod.addValueChangeListener(event -> {
            PaymentMethod pm = paymentMethod.getValue();
            if (pm != null) {
                Notification notification = new Notification("Guaranty deposit changed " +
                        pm.getOldValue() + "% ---> " + event.getValue() + "%" +
                        "\n Press save!");
                notification.setPosition(Position.BOTTOM_RIGHT);
                notification.setDelayMsec(1000);
                notification.show(Page.getCurrent());
            }
        });*/

        HorizontalLayout buttons = new HorizontalLayout(save, cancel);
        addComponents(name, address, rating, operatesFrom, category, paymentMethod, url, description, buttons);

        category.setItemCaptionGenerator(CategoryEntity::getName);

        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        cancel.setStyleName(ValoTheme.BUTTON_DANGER);

        setFieldsId();
        setTooltips();
        bindFields();
    }

    public NativeSelect<CategoryEntity> getCategory() {
        return category;
    }

    //Задаются тултипы для каждого поля
    @Override
    protected void setTooltips() {
        name.setDescription("Name of hotel");
        address.setDescription("Address of hotel");
        rating.setDescription("Rating of hotel");
        operatesFrom.setDescription("Date when the hotel opened");
        category.setDescription("CategoryEntity of hotel");
        url.setDescription("Reference to booking.com");
        description.setDescription("Some information about hotel");
    }

    //Связываются свойства бина с полями формы, устанавливается валидация и конвертация полей/свойств
    @Override
    protected void bindFields() {
        binder.forField(name)
                .asRequired("Field is required").withNullRepresentation("")
                .bind(HotelEntity::getName, HotelEntity::setName);

        binder.forField(address)
                .asRequired("Field is required").withNullRepresentation("")
                .bind(HotelEntity::getAddress, HotelEntity::setAddress);

        binder.forField(rating)
                .asRequired("Field is required").withNullRepresentation("")
                .withConverter(new StringToIntegerConverter(0, "Must enter an integer"))
                .withValidator(r -> r < 6 && r > 0, "Rating should be a positive number less than 6")
                .bind(HotelEntity::getRating, HotelEntity::setRating);

        binder.forField(operatesFrom)
                .asRequired("Field is required").withNullRepresentation(null)
                .withConverter(new DateConverter())
                .withValidator(date -> date > -1, "Date should be no later than today")
                .withValidator(date -> date < DAYS.between(LocalDate.parse("1900-01-01"), LocalDate.now()), "Date should begin by the 20th century")
                .bind(HotelEntity::getOperatesFrom, HotelEntity::setOperatesFrom);

        binder.forField(category)
                .asRequired("Field is required")
                .withValidator(hotelCategory -> !hotelCategory.getName().isEmpty(), "CategoryEntity cannot be empty")
                .bind(HotelEntity::getCategory, HotelEntity::setCategory);

        binder.forField(paymentMethod)
                .withNullRepresentation(new PaymentMethod())
                .bind(HotelEntity::getPaymentMethod, HotelEntity::setPaymentMethod);

        binder.forField(url)
                .asRequired("Field is required").withNullRepresentation("")
                .bind(HotelEntity::getUrl, HotelEntity::setUrl);

        binder.forField(description).bind(HotelEntity::getDescription, HotelEntity::setDescription);
    }

    private void setFieldsId() {
        name.setId("name");
        address.setId("address");
        rating.setId("rating");
        operatesFrom.setId("operatesFrom");
        category.setId("category");
        paymentMethod.setId("paymentMethod");
        url.setId("url");
        description.setId("description");
    }

    //Binder устанавливает связь с выбранным бином
    @Override
    public void setBean(HotelEntity bean) {
        this.hotel = bean;
        binder.readBean(hotel);
        setVisible(true);
        name.selectAll();
    }

    //Если форма не проходит валидацию, то появляется уведомление
    @Override
    protected void save() {
        try {
            binder.validate();
            binder.writeBean(hotel);
            if (hotel.getId() == null)
                view.getHotelService().add(hotel);
            else
                view.getHotelService().update(hotel);
            setVisible(false);
        } catch (ValidationException e) {
            view.createNotification("Fill form fields correctly", Position.TOP_RIGHT).show(Page.getCurrent());
        }
    }
}
package com.nidhogg.forms;

import com.nidhogg.views.HotelCategoryView;
import com.nidhogg.entity.CategoryEntity;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Created by Arthur on 17.05.2017.
 */
public class CategoryForm extends AbstractForm<CategoryEntity> {

    private HotelCategoryView view;
    private TextField name = new TextField("Name");
    private CategoryEntity category;

    public CategoryForm(HotelCategoryView view) {
        this.view = view;
        binder = new Binder<>(CategoryEntity.class);

        setSizeUndefined();

        name.setId("categoryNameField");

        Button save = new Button("Save");
        save.setId("saveCategoryFormBtn");

        Button cancel = new Button("Cancel");

        save.addClickListener(clickEvent -> {
            save();
            view.updateList();
        });

        cancel.addClickListener(clickEvent -> setVisible(false));


        HorizontalLayout buttons = new HorizontalLayout(save, cancel);
        addComponents(name, buttons);

        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        cancel.setStyleName(ValoTheme.BUTTON_DANGER);

        setTooltips();
        bindFields();
    }


    //Задаются тултипы для поля
    @Override
    protected void setTooltips() {
        name.setDescription("Name of hotel category");
    }

    //Связываются свойства бина с полями формы, устанавливается валидация и конвертация полей/свойств
    @Override
    protected void bindFields() {
        binder.forField(name)
                .asRequired("Field cannot be empty")
                .withValidator(s -> {
                    for (CategoryEntity hc : view.getCategoryService().findAll()) {
                        if (s.equals(hc.getName()))
                            return false;
                    }
                    return true;
                }, "This category already exists")
                .bind(CategoryEntity::getName, CategoryEntity::setName);
    }

    //Устанавливаются значения полей в форме из объекта CategoryEntity, находящегося в Set categories
    @Override
    public void setBean(CategoryEntity bean) {
        this.category = bean;
        binder.readBean(category);
        setVisible(true);
    }

    @Override
    protected void save() {
        try {
            binder.validate();
            binder.writeBean(category);
            if (category.getId() == null)
                view.getCategoryService().add(category);
            else
                view.getCategoryService().update(category);
            setVisible(false);
        } catch (ValidationException e) {
            view.createNotification("Fill form fields correctly!", Position.TOP_RIGHT).show(Page.getCurrent());
        }
    }
}

package com.nidhogg;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.server.SerializablePredicate;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractField;
import com.vaadin.data.Validator;
import com.vaadin.ui.Button;

import java.util.function.Predicate;

/**
 * Created by Arthur on 16.05.2017.
 */
public class Utils {
    public static void addValidator(AbstractField field, Validator validator, Button button) {
        field.addValueChangeListener(event -> {
            ValidationResult result = validator.apply(event.getValue(), new ValueContext(field));

            if (result.isError()) {
                UserError error = new UserError(result.getErrorMessage());
                button.setEnabled(false);
                field.setComponentError(error);
            } else {
                field.setComponentError(null);
                button.setEnabled(true);
            }
        });
    }
}

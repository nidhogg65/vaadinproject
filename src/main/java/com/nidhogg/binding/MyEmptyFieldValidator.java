package com.nidhogg.binding;

import com.vaadin.data.ValidationResult;
import com.vaadin.data.ValueContext;
import com.vaadin.data.validator.AbstractValidator;


/**
 * Created by Arthur on 05.05.2017.
 */
public class MyEmptyFieldValidator extends AbstractValidator<String> {

    public MyEmptyFieldValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public ValidationResult apply(String s, ValueContext valueContext) {
        if (s.trim().isEmpty())
            return ValidationResult.error(getMessage(s));
        else
            return ValidationResult.ok();
    }
}

package com.nidhogg.binding;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

public class DateConverter implements Converter<LocalDate, Long> {

    @Override
    public Result<Long> convertToModel(LocalDate date, ValueContext valueContext) {

        if (date == null)
            return Result.ok(null);
        try {
            return Result.ok(DAYS.between(date, LocalDate.now()));
        } catch (NumberFormatException e) {
            return Result.error("Wrong format!");
        }
    }

    @Override
    public LocalDate convertToPresentation(Long value, ValueContext valueContext) {

        if (value == null)
            return LocalDate.now();
        return LocalDate.now().minusDays(value);
    }

    public Long convertToModel(LocalDate date) {
        return DAYS.between(date, LocalDate.now());
    }
}

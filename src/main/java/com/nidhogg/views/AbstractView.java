package com.nidhogg.views;

import com.nidhogg.service.CategoryService;
import com.nidhogg.service.HotelService;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;


/**
 * Created by Arthur on 17.05.2017.
 */

public class AbstractView extends VerticalLayout implements View {

    private HotelService hotelService;
    private CategoryService categoryService;

    public AbstractView(HotelService hotelService, CategoryService categoryService) {
        super();
        this.hotelService = hotelService;
        this.categoryService = categoryService;
    }

    public HotelService getHotelService() {
        return hotelService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    //Создает уведомление
    public Notification createNotification(String message, Position position) {
        Notification notification = new Notification(message);
        notification.setPosition(position);
        notification.setDelayMsec(2000);
        return notification;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }
}

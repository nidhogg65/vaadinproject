package com.nidhogg.views;

import com.nidhogg.Utils;
import com.nidhogg.binding.DateConverter;
import com.nidhogg.binding.MyEmptyFieldValidator;
import com.nidhogg.entity.CategoryEntity;
import com.nidhogg.entity.HotelEntity;
import com.nidhogg.forms.HotelForm;
import com.nidhogg.service.CategoryService;
import com.nidhogg.service.HotelService;
import com.vaadin.event.ShortcutAction;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;


/**
 * Created by Arthur on 05.05.2017.
 */

@Component
public class HotelView extends AbstractView {

    private Grid<HotelEntity> grid = new Grid<>(HotelEntity.class);
    private TextField nameFilterText = new TextField();
    private TextField addressFilterText = new TextField();
    private HotelForm form = new HotelForm(this);
    //PopupView и его контент
    private PopupContent content = new PopupContent();
    private PopupView popup;
    //Отели, выбранные мультиселектом
    private Set<HotelEntity> hotelSet = new HashSet<>();
    private Notification notification;

    @Autowired
    public HotelView(HotelService hotelService, CategoryService categoryService) {
        super(hotelService, categoryService);
        init();
    }

    private void init() {
        //Фильтры и кнопки их сброса
        nameFilterText.setPlaceholder("filter by name...");
        nameFilterText.addValueChangeListener(e -> updateListWithFilters());
        nameFilterText.setValueChangeMode(ValueChangeMode.LAZY);

        Button clearNameFilterBtn = new Button(VaadinIcons.CLOSE);
        clearNameFilterBtn.setDescription("Clear name filter");
        clearNameFilterBtn.addClickListener(clickEvent -> nameFilterText.clear());

        addressFilterText.setPlaceholder("filter by address...");
        addressFilterText.addValueChangeListener(clickEvent -> updateListWithFilters());
        addressFilterText.setValueChangeMode(ValueChangeMode.LAZY);

        Button clearAddressFilterBtn = new Button(VaadinIcons.CLOSE);
        clearAddressFilterBtn.setDescription("Clear address filter");
        clearAddressFilterBtn.addClickListener(clickEvent -> addressFilterText.clear());


        CssLayout filtering = new CssLayout();
        filtering.addComponents(nameFilterText, clearNameFilterBtn, addressFilterText, clearAddressFilterBtn);
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        //Кнопки
        Button newBtn = new Button("New");
        Button editBtn = new Button("Edit");
        Button deleteBtn = new Button("Delete");
        Button bulkUpdateBtn = new Button("Bulk Update");

        newBtn.setId("newBtn");

        //Убираем доступ к кнопкам при запуске приложения
        editBtn.setEnabled(false);
        deleteBtn.setEnabled(false);
        bulkUpdateBtn.setEnabled(false);

        //Добавляем слушателей к кнопкам
        newBtn.addClickListener(clickEvent -> {
            grid.asMultiSelect().clear();
            form.setBean(new HotelEntity());
        });
        editBtn.addClickListener(clickEvent -> hotelSet.forEach(form::setBean));
        deleteBtn.addClickListener(clickEvent -> {
            deleteHotels();
            updateList();
        });
        bulkUpdateBtn.addClickListener(clickEvent -> {
            popup.setPopupVisible(true);
        });

        //Создание и настройка PopupView компонента
        popup = new PopupView(content);
        popup.setHideOnMouseOut(false);
        popup.addPopupVisibilityListener(event -> {
            content.textField.clear();
            content.rating.clear();
            content.operatesFrom.clear();
            content.category.clear();
            content.description.clear();
            content.updateBtn.setEnabled(false);
        });

        HorizontalLayout buttons = new HorizontalLayout(newBtn, editBtn, deleteBtn, bulkUpdateBtn, popup);

        initGridComponent();
        grid.asMultiSelect().addValueChangeListener(valueChangeEvent -> {
            Set<HotelEntity> set = valueChangeEvent.getValue();
            hotelSet.clear();
            hotelSet.addAll(set);
            editBtn.setEnabled(hotelSet.size() == 1);
            deleteBtn.setEnabled(hotelSet.size() > 0);
            bulkUpdateBtn.setEnabled(hotelSet.size() > 1);

        });

        HorizontalLayout hotelLayout = new HorizontalLayout(grid, form);
        hotelLayout.setSizeFull();
        hotelLayout.setExpandRatio(grid, 1);


        addComponents(filtering, buttons, hotelLayout);
        updateList();
        form.setVisible(false);

        notification = createNotification("Hello! This is hotel list!", Position.TOP_CENTER);
    }

    public void updateList() {
        List<HotelEntity> hotels = getHotelService().findAll();
        grid.setItems(hotels);
    }

    public void updateListWithFilters() {
        List<HotelEntity> hotels = getHotelService().findAll(nameFilterText.getValue(), addressFilterText.getValue());
        grid.setItems(hotels);
    }

    //Настройка компонента Grid
    private void initGridComponent() {
        grid.setSizeFull();
        grid.setHeightByRows(10);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.setColumns("name", "address", "rating", "operatesFrom", "category", "description");
        Grid.Column<HotelEntity, String> url = grid.addColumn(hotel -> "<a href='" + hotel.getUrl() + "'target='_blank'>ref</a>",
                new HtmlRenderer());
        url.setCaption("Url");
    }

    //Удаляет выбранные отели
    private void deleteHotels() {
        for (HotelEntity selectedHotel : hotelSet) {
            getHotelService().remove(selectedHotel);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        form.setVisible(false);
        updateList();
        List<CategoryEntity> categories = getCategoryService().findAll();
        content.category.setItems(categories);
        form.getCategory().setItems(categories);
        notification.show(Page.getCurrent());
    }


    private class PopupContent implements PopupView.Content {

        private final VerticalLayout popupContent;
        private Label caption = new Label("BULK UPDATE");
        private ComboBox<String> fieldBox = new ComboBox<>();
        private TextField textField = new TextField();
        private TextField rating = new TextField();
        private DateField operatesFrom = new DateField();
        private NativeSelect<CategoryEntity> category = new NativeSelect<>();
        private TextArea description = new TextArea();

        private Button updateBtn = new Button("Update");
        private Button cancelBtn = new Button("Cancel");

        private PopupContent() {

            popupContent = new VerticalLayout();

            settingComponents();

            setComponentsVisible();
            addValidators();

            updateBtn.setEnabled(false);
            updateBtn.setStyleName(ValoTheme.BUTTON_PRIMARY);
            updateBtn.setClickShortcut(ShortcutAction.KeyCode.ENTER);
            cancelBtn.setStyleName(ValoTheme.BUTTON_DANGER);

            updateBtn.addClickListener(clickEvent -> {
                updateHotels(HotelView.this.hotelSet, fieldBox.getValue());
                updateList();
                popup.setPopupVisible(false);
            });
            cancelBtn.addClickListener(clickEvent -> popup.setPopupVisible(false));

            HorizontalLayout buttons = new HorizontalLayout(updateBtn, cancelBtn);

            setVisibleFieldDependingOnTheChoice();

            popupContent.addComponents(caption, fieldBox, textField, rating, operatesFrom, category, description, buttons);

        }

        //Настройка компонентов
        private void settingComponents() {
            List<String> namesOfFields = Arrays.asList("Name", "Address", "Rating",
                    "Operates from", "Category", "URL", "Description");
            caption = new Label("BULK UPDATE");
            caption.setStyleName(ValoTheme.LABEL_LARGE);
            fieldBox.setEmptySelectionAllowed(false);
            fieldBox.setPlaceholder("Select field please");
            fieldBox.setItems(namesOfFields);
            textField.setPlaceholder("Field value");
            rating.setPlaceholder("Rating");
            category.setEmptySelectionAllowed(false);
            description.setPlaceholder("Description");
            description.setRows(2);
        }

        //Добавление валидаторов на соответствующие компоненты
        private void addValidators() {
            //На TextField для полей name, address, url
            Utils.addValidator(textField, new MyEmptyFieldValidator("Field cannot be empty"), updateBtn);
            //На TextField для поля Rating
            rating.addValueChangeListener(e -> {
                try {
                    int x = Integer.parseInt(rating.getValue());
                    if (x > 0 && x < 6) {
                        rating.setComponentError(null);
                        updateBtn.setEnabled(true);
                    } else {
                        rating.setComponentError(new UserError("Rating should be a positive number less than 6"));
                        updateBtn.setEnabled(false);
                    }
                } catch (NumberFormatException ex) {
                    rating.setComponentError(new UserError("That's not a integer"));
                    updateBtn.setEnabled(false);
                }
            });
            //На ComboBox для поля category
            category.addValueChangeListener(e -> updateBtn.setEnabled(true));
            //На DateField для поля operates from
            operatesFrom.addValueChangeListener(e -> {
                if (operatesFrom.getValue() != null) {
                    long days = DAYS.between(operatesFrom.getValue(), LocalDate.now());
                    if (days < 0) {
                        operatesFrom.setComponentError(new UserError("Date should be no later than today"));
                        updateBtn.setEnabled(false);
                    } else if (days > DAYS.between(LocalDate.parse("1900-01-01"), LocalDate.now())) {
                        operatesFrom.setComponentError(new UserError("Date should begin by the 20th century"));
                        updateBtn.setEnabled(false);
                    } else {
                        operatesFrom.setComponentError(null);
                        updateBtn.setEnabled(true);
                    }
                }
            });
        }

        //В этом методе происходит апдейт полей у выбранных отелей
        private void updateHotels(Set<HotelEntity> set, String field) {
            for (HotelEntity hotelEntity : set) {
                switch (field) {
                    case "Name":
                        hotelEntity.setName(textField.getValue());
                        break;
                    case "Address":
                        hotelEntity.setAddress(textField.getValue());
                        break;
                    case "URL":
                        hotelEntity.setUrl(textField.getValue());
                        break;
                    case "Rating":
                        hotelEntity.setRating(Integer.parseInt(rating.getValue()));
                        break;
                    case "Operates from":
                        hotelEntity.setOperatesFrom(new DateConverter().convertToModel(operatesFrom.getValue()));
                        break;
                    case "Category":
                        CategoryEntity categoryByID = getCategoryService().findById(category.getValue().getId());
                        hotelEntity.setCategory(categoryByID);
                        break;
                    case "Description":
                        hotelEntity.setDescription(description.getValue());
                        break;
                }
                getHotelService().update(hotelEntity);
            }
        }

        //Отображение нужного типа поля в зависимости от выбора
        private void setVisibleFieldDependingOnTheChoice() {
            fieldBox.addValueChangeListener(valueChangeEvent -> {
                String name = fieldBox.getValue();
                switch (name) {
                    case "Name":
                    case "Address":
                    case "URL":
                        textField.setVisible(true);
                        rating.setVisible(false);
                        category.setVisible(false);
                        operatesFrom.setVisible(false);
                        description.setVisible(false);
                        updateBtn.setEnabled(false);
                        break;
                    case "Rating":
                        textField.setVisible(false);
                        rating.setVisible(true);
                        category.setVisible(false);
                        operatesFrom.setVisible(false);
                        description.setVisible(false);
                        updateBtn.setEnabled(false);
                        break;
                    case "Operates from":
                        textField.setVisible(false);
                        rating.setVisible(false);
                        category.setVisible(false);
                        operatesFrom.setVisible(true);
                        description.setVisible(false);
                        updateBtn.setEnabled(false);
                        break;
                    case "Category":
                        textField.setVisible(false);
                        rating.setVisible(false);
                        category.setVisible(true);
                        operatesFrom.setVisible(false);
                        description.setVisible(false);
                        updateBtn.setEnabled(false);
                        break;
                    case "Description":
                        textField.setVisible(false);
                        rating.setVisible(false);
                        category.setVisible(false);
                        operatesFrom.setVisible(false);
                        description.setVisible(true);
                        updateBtn.setEnabled(true);
                        break;
                }
            });
        }

        //Делает невидимым все компоненты кроме textField для name, address и url
        private void setComponentsVisible() {
            textField.setVisible(true);
            rating.setVisible(false);
            operatesFrom.setVisible(false);
            category.setVisible(false);
            description.setVisible(false);
        }

        @Override
        public final com.vaadin.ui.Component getPopupComponent() {
            return popupContent;
        }

        @Override
        public final String getMinimizedValueAsHTML() {
            return null;
        }
    }
}
package com.nidhogg.views;

import com.nidhogg.entity.CategoryEntity;
import com.nidhogg.forms.CategoryForm;
import com.nidhogg.service.CategoryService;
import com.nidhogg.service.HotelService;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Arthur on 05.05.2017.
 */
@Component
public class HotelCategoryView extends AbstractView {

    private Grid<CategoryEntity> grid = new Grid<>(CategoryEntity.class);
    private CategoryForm form = new CategoryForm(this);
    private Set<CategoryEntity> categorySet = new HashSet<>();
    private Notification notification;

    @Autowired
    public HotelCategoryView(HotelService hotelService, CategoryService categoryService) {
        super(hotelService, categoryService);
        init();
    }

    //Создание элементов, которые будут находиться в этом View
    private void init() {
        Button newBtn = new Button("New");
        newBtn.setId("newCategoryBtn");

        Button editBtn = new Button("Edit");
        Button deleteBtn = new Button("Delete");

        editBtn.setEnabled(false);
        deleteBtn.setEnabled(false);

        newBtn.addClickListener(clickEvent -> {
            grid.asMultiSelect().clear();
            form.setBean(new CategoryEntity());
        });
        editBtn.addClickListener(clickEvent -> categorySet.forEach(form::setBean));
        deleteBtn.addClickListener(clickEvent -> {
            deleteCategories();
            updateList();
        });

        HorizontalLayout toolbar = new HorizontalLayout(newBtn, editBtn, deleteBtn);

        //Настройка компонента Grid
        initGridComponent();
        grid.asMultiSelect().addValueChangeListener(valueChangeEvent -> {
            Set<CategoryEntity> set = valueChangeEvent.getValue();
            categorySet.clear();
            categorySet.addAll(set);
            //Скрывает кнопку delete, если количество выбранных категорий соответствует количество категорий в общем
            //(т.е. нельзя удалить все категории сразу и последнюю оставшуюся категорию)
            editBtn.setEnabled(categorySet.size() == 1);
            deleteBtn.setEnabled(!(categorySet.size() == getCategoryService().findAll().size()) && categorySet.size() > 0);

        });

        HorizontalLayout categoryLayout = new HorizontalLayout(grid, form);
        categoryLayout.setSizeFull();

        addComponents(toolbar, categoryLayout);
        updateList();
        form.setVisible(false);

        notification = createNotification("Hello! This is category list!", Position.TOP_CENTER);
    }

    //Настройка компонента Grid
    private void initGridComponent() {
        grid.setHeightByRows(10);
        grid.setColumns("name");
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
    }

    public void updateList() {
        List<CategoryEntity> categories = getCategoryService().findAll();
        grid.setItems(categories);
    }

    //Удаляет выбранные категории
    private void deleteCategories() {
        for (CategoryEntity hotelCategory : categorySet) {
            getCategoryService().remove(hotelCategory);
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        form.setVisible(false);
        updateList();
        notification.show(Page.getCurrent());
    }
}
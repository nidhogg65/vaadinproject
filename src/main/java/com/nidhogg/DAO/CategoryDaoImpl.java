package com.nidhogg.DAO;

import com.nidhogg.entity.CategoryEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arthur on 13.05.2017.
 */

@Repository
public class CategoryDaoImpl implements CategoryDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<CategoryEntity> findAll() {
        return em.createNamedQuery("CategoryEntity.getAll", CategoryEntity.class).getResultList();
    }

    @Override
    public CategoryEntity findById(Long id) {
        return em.find(CategoryEntity.class, id);
    }

    @Override
    public void add(CategoryEntity entity) {
        em.persist(entity);
    }

    @Override
    public void update(CategoryEntity entity) {
        em.merge(entity);
    }

    @Override
    public void remove(CategoryEntity entity) {
        CategoryEntity categoryFromDB = findById(entity.getId());
        if (categoryFromDB != null) {
            em.remove(categoryFromDB);
        }
    }
}

package com.nidhogg.DAO;

import com.nidhogg.entity.HotelEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Arthur on 13.05.2017.
 */

@Repository
public class HotelDaoImpl implements HotelDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<HotelEntity> findAll() {
        return em.createNamedQuery("HotelEntity.getAll", HotelEntity.class).getResultList();
    }

    @Override
    public List<HotelEntity> findAll(String name, String address) {
        Query query = em.createQuery("select h from HotelEntity h WHERE name LIKE :name AND address LIKE :address");
        query.setParameter("name", "%" + name + "%");
        query.setParameter("address", "%" + address + "%");
        return query.getResultList();
    }

    @Override
    public HotelEntity findById(Long id) {
        return em.find(HotelEntity.class, id);
    }

    @Override
    public void add(HotelEntity entity) {
        em.persist(entity);
    }

    @Override
    public void update(HotelEntity entity) {
        em.merge(entity);
    }

    @Override
    public void remove(HotelEntity entity) {
        HotelEntity hotelFromDB = findById(entity.getId());
        if (hotelFromDB != null) {
            em.remove(hotelFromDB);
        }
    }

    public void refreshHotels() {
        List<HotelEntity> hotels = findAll();
        for (HotelEntity hotel : hotels) {
            em.refresh(hotel);
        }
    }
}

package com.nidhogg.DAO;

import com.nidhogg.entity.CategoryEntity;

import java.util.List;

/**
 * Created by Arthur on 21.07.2017.
 */
public interface CategoryDao {

    List<CategoryEntity> findAll();

    CategoryEntity findById(Long id);

    void add(CategoryEntity entity);

    void update(CategoryEntity entity);

    void remove(CategoryEntity entity);

}

package com.nidhogg.service;

import com.nidhogg.DAO.CategoryDao;
import com.nidhogg.entity.CategoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Arthur on 13.05.2017.
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryDao dao;

    @Autowired
    public CategoryServiceImpl(CategoryDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public List<CategoryEntity> findAll() {
        return dao.findAll();
    }

    @Override
    @Transactional
    public CategoryEntity findById(Long id) {
        return dao.findById(id);
    }

    @CacheEvict(value = "hotels", beforeInvocation = true, allEntries = true)
    @Override
    @Transactional
    public void add(CategoryEntity entity) {
        dao.add(entity);
    }

    @CacheEvict(value = "hotels", beforeInvocation = true, allEntries = true)
    @Override
    @Transactional
    public void update(CategoryEntity entity) {
        dao.update(entity);
    }

    @CacheEvict(value = "hotels", beforeInvocation = true, allEntries = true)
    @Override
    @Transactional
    public void remove(CategoryEntity entity) {
        dao.remove(entity);
    }


}

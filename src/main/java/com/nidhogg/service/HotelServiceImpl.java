package com.nidhogg.service;

import com.nidhogg.DAO.HotelDao;
import com.nidhogg.entity.HotelEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Arthur on 13.05.2017.
 */
@Service
public class HotelServiceImpl implements HotelService {

    private HotelDao dao;

    @Autowired
    public HotelServiceImpl(HotelDao dao) {
        this.dao = dao;
    }

    @Cacheable("hotels")
    @Override
    @Transactional
    public List<HotelEntity> findAll() {
        return dao.findAll();
    }

    @Cacheable("hotels")
    @Override
    @Transactional
    public List<HotelEntity> findAll(String name, String address) {
        return dao.findAll(name, address);
    }

    @Override
    @Transactional
    public HotelEntity findById(Long id) {
        return dao.findById(id);
    }

    @CacheEvict(value = "hotels", beforeInvocation = true, allEntries = true)
    @Override
    @Transactional
    public void add(HotelEntity entity) {
        dao.add(entity);
    }

    @CacheEvict(value = "hotels", beforeInvocation = true, allEntries = true)
    @Override
    @Transactional
    public void update(HotelEntity entity) {
        dao.update(entity);
    }

    @CacheEvict(value = "hotels", beforeInvocation = true, allEntries = true)
    @Override
    @Transactional
    public void remove(HotelEntity entity) {
        dao.remove(entity);
    }

}

package com.nidhogg.service;

import com.nidhogg.entity.HotelEntity;
import java.util.List;

/**
 * Created by Arthur on 21.07.2017.
 */
public interface HotelService {

    List<HotelEntity> findAll();

    List<HotelEntity> findAll(String name, String address);

    HotelEntity findById(Long id);

    void add(HotelEntity entity);

    void update(HotelEntity entity);

    void remove(HotelEntity entity);
}

package com.nidhogg.UI;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import com.nidhogg.views.HotelCategoryView;
import com.nidhogg.views.HotelView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */

@Theme("mytheme")
@SpringUI
public class MyUI extends UI {

    private Navigator navigator;

    private HotelView hotelView;
    private HotelCategoryView categoryView;

    @Autowired
    public void setHotelView(HotelView hotelView) {
        this.hotelView = hotelView;
    }

    @Autowired
    public void setCategoryView(HotelCategoryView categoryView) {
        this.categoryView = categoryView;
    }

    @WebListener
    public static class MyContextLoaderListener extends ContextLoaderListener {
    }

    @Configuration
    @EnableVaadin
    public static class MyConfiguration {
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        final VerticalLayout mainLayout = new VerticalLayout();
        //Панель содержащая View
        Panel panel = new Panel();

        MenuBar menu = new MenuBar();
        MenuBar.Command goToHotelList = menuItem -> navigator.navigateTo("hotels");
        MenuBar.Command goToHotelCategories = menuItem -> navigator.navigateTo("categories");

        menu.addItem("Hotel List", null, goToHotelList);
        menu.addItem("Hotel Categories", null, goToHotelCategories);

        mainLayout.addComponents(menu, panel);
        setContent(mainLayout);

        navigator = new Navigator(this, panel);
        navigator.addView("", hotelView);
        navigator.addView("hotels", hotelView);
        navigator.addView("categories", categoryView);

    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends SpringVaadinServlet {
    }
}
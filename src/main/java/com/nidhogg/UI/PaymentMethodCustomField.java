package com.nidhogg.UI;

import com.nidhogg.entity.PaymentMethod;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Arthur on 19.05.2017.
 */
public class PaymentMethodCustomField extends CustomField<PaymentMethod> {

    private String caption;
    private RadioButtonGroup<String> radioBtnGroup = new RadioButtonGroup<>();
    private List<String> list = Arrays.asList("Credit Card", "Cash");
    private TextField field = new TextField();
    private Label noCard1 = new Label("Payment will be made");
    private Label noCard2 = new Label("directly in the hotel");

    private Binder<PaymentMethod> binder = new Binder<>(PaymentMethod.class);

    private PaymentMethod value;
    private Integer oldGuarantyFee;
    private boolean isCash = false;



    public PaymentMethodCustomField(String caption) {
        super();
        this.caption = caption;
    }

    @Override
    protected Component initContent() {

        VerticalLayout main = new VerticalLayout();
        super.setCaption(caption);

        field.setDescription("Guaranty Deposit");
        field.setPlaceholder("Guaranty Deposit");

        //Надо фиксить
        //field.addValueChangeListener(this::fireEvent);

        noCard1.setStyleName(ValoTheme.LABEL_SMALL);
        noCard2.setStyleName(ValoTheme.LABEL_SMALL);
        field.setVisible(false);
        noCard1.setVisible(false);
        noCard2.setVisible(false);

        setRadioButton();

        bindField();

        main.addComponents(radioBtnGroup, field, noCard1, noCard2);

        return main;
    }

    private void setRadioButton() {
        radioBtnGroup.setItems(list);
        radioBtnGroup.setItemCaptionGenerator(String::toString);
        radioBtnGroup.setItemIconGenerator(s -> {
                    if (list.get(0).equals(s))
                        return VaadinIcons.CREDIT_CARD;
                    else return VaadinIcons.CASH;
                }
        );

        //Скрывает/показывает поля и лэйблы в зависимости от выбора
        radioBtnGroup.addSelectionListener(event -> {
            if (radioBtnGroup.getSelectedItem().get().equals(list.get(0))) {
                if (isCash)
                    setValue(new PaymentMethod(oldGuarantyFee));
                field.setVisible(true);
                noCard1.setVisible(false);
                noCard2.setVisible(false);
            } else {
                isCash = true;
                oldGuarantyFee = value.getGuarantyFee();
                value = null;
                field.setVisible(false);
                noCard1.setVisible(true);
                noCard2.setVisible(true);
            }
            binder.setBean(value);
        });
    }

    //Bind textfield
    private void bindField() {
        binder.forField(field)
                .withConverter(new StringToIntegerConverter("Must enter an integer"))
                .withValidator(guaranty -> guaranty == Integer.MIN_VALUE || guaranty >= 0 && guaranty <= 100, "Guaranty should be >= 0 and <= 100")
                .bind(PaymentMethod::getGuarantyFee, PaymentMethod::setGuarantyFee);
    }

    private void updateValues() {
        if (getValue() != null) {
            if (isCash) {
                radioBtnGroup.setSelectedItem(list.get(1));
            } else {
                radioBtnGroup.setSelectedItem(list.get(0));
            }
        }
    }

    @Override
    protected void doSetValue(PaymentMethod pm) {
        if (pm.getGuarantyFee() == null) {
            setValue(new PaymentMethod(0));
            isCash = true;
            field.clear();
        } else {
            this.value = new PaymentMethod(pm.getGuarantyFee());
            isCash = false;
        }
        updateValues();
    }

    @Override
    public PaymentMethod getValue() {
        return value;
    }
}